

create database [Millionaire]

USE [Millionaire]

CREATE TABLE [dbo].[Admin](
	[UserName] [nvarchar](50) NOT NULL  PRIMARY KEY,
	[PassWord] [nvarchar](50) NULL,
	[Hoten] [nvarchar](50) NULL
 
) 


CREATE TABLE [dbo].[Player](
	[IdPlayer] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[UserName] [nvarchar](50) NULL,
	[PassWord] [nvarchar](50) NULL,
	[Money] [int] NULL
 
) 


CREATE TABLE [dbo].[Question](
	[MaQT] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Question] [nvarchar](300) NULL,
	[AnswerA] [nvarchar](100) NULL,
	[AnswerB] [nvarchar](100) NULL,
	[AnswerC] [nvarchar](100) NULL,
	[AnswerD] [nvarchar](100) NULL,
	[Answer] [nvarchar](100) NULL

) 

GO
INSERT [dbo].[Admin] ([UserName], [PassWord], [Hoten]) VALUES (N'admin', N'123456', N'Oanh')
GO
SET IDENTITY_INSERT [dbo].[Player] ON 

GO
INSERT [dbo].[Player] ([IdPlayer], [UserName], [PassWord], [Money]) VALUES (1, N'Oanh', N'123456', NULL)
GO
SET IDENTITY_INSERT [dbo].[Player] OFF
GO
SET IDENTITY_INSERT [dbo].[Question] ON 

GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (1, N'Trong  truyên ngắn” ông lão đánh cá và con cá vàng ” , ông lão đã ra biển mấy lần', N'3', N'4', N'6', N'5', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (2, N' Ai Là Người đầu tiên  chứng minh trái đất có dạng hình cầu ?', N'Copecnich ', N'Aristotle ', N' Acsimet', N'Galileo  ', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (3, N'Đội Bóng xếp thứ 3 giải  Ngoại hạng anh mùa bóng 2006-2007 ?', N'Liverpool', N'Manchester United  ', N'Asenal', N'Chelsea', N'A')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (4, N'Sông Mekong bắt nguồn từ đâu ?', N'Trung Quốc', N' Lào ', N'Campuchia', N'Việt Nam', N'A')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (5, N'Một Mét Khối nước tương đương  với bao nhiêu lít nươc ?', N'10', N'100', N'10.000', N'1000', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (6, N'Cho đến năm 2007  Thái Lan Đã tổ chức bao nhiêu lần đại hội  SEA Games ?', N'3', N'4', N'5', N'6', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (7, N' Ở Mắt bão không khí  chuyển động  như thế nào ?', N'Theo chiều kim đồng hồ  ', N' không Chuyển Động ', N' Ngược Chiều kim Đồng Hồ  ', N'Tuỳ vào Cơn Bảo', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (8, N'Ai Đã khắc chữ ” Nam  thiên đệ Nhất động “lên của động hương tích ?', N'Chúa Trịnh Sâm', N'Chu Mạnh Trinh', N'Vua Bảo Đại    ', N'Nam Phương Hoàng hậu  ', N'A')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (9, N'Nơi có hải cảng sầm uất nhất thế giới , bạn cho biết tên  thành phố này ?', N' London  ', N'Thượng Hải  ', N' New York', N'Trân Châu Cảng', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (13, N'Đỉnh núi Cao Nhất nước ta  Pan Xi păng Thuộc tỉnh nào ?', N'Sơn la  ', N'Lào Cai', N'Cao bằng', N'Bắc Cạn', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (14, N'Có bao nhiêu vị trạng nguyên dưới thời vua gia long ?', N'1', N'2', N'Không có Trạng Nguyên', N'3', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (15, N'Quốc Gia  Có Lượng Khí thải lớn Nhất Thế Giới ?', N'Đức ', N'Mỹ ', N'Australia', N'Anh', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (16, N'Đại dương có diện tích nhỏ nhất trên thế giới ?', N'Bắc Băng Dương   ', N'Đại tây Dương ', N'Ấn Độ Dương', N'Thái Bình Dương', N'A')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (17, N'Quốc Kỳ Cộng hoà Liên Bang Đức Có Bao Nhiêu Màu ?', N'2', N'3', N'4', N'5', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (18, N'Vào những năm 1850 – 1870 , Nước nào có nền công nghiệp đứng đầu thế giới và được coi là” công xưởng của thế giới ?', N'Đức', N'Pháp', N'Mỹ         ', N'Anh  ', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (19, N'Dưới thời nhà trần , ai là thầy giáo , nhà nho được nhiều người  trọng dụng nhất ?', N'Chu Văn An', N' Trương Hán Siêu    ', N'Nguyễn Trãi   ', N'Phạm Sư Mạnh   ', N'A')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (20, N' Dơi phát ra âm thanh từ bộ phận nào của cơ thể ?', N'Cánh ', N'Mũi', N'Cổ Họng  ', N'Răng', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (21, N' Lò phản ứng hạt nhân của nước ta đầu tiên nằm ở đâu ?', N'Hà Nội   ', N'Cần Thơ  ', N'Đà Lạt ', N'Đồng Nai', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (22, N'Cầu thang lên Chùa Một Cột  hiện Nay làm bằng gì ?', N'Gỗ  ', N'Tre', N'Gạch,Vữa ', N'Bê Tông ', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (23, N'Người ta cho thêm chì  vào trong xăng Nhằm mục đích gì ?', N'tăng Thề Tích Của xăng', N'Chống Cháy Nổ  ', N'Giảm Ô nhiễm Môi Trường     ', N'Giảm Bay Hơi   ', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (24, N'Nhạc Cụ Gõ Cổ Nhất Của Nước ta Tên Là Gì ?', N' Không Phải 3 loại Trên   ', N'Trống Đồng  ', N'Trống Đá    ', N'Đàn Đá   ', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (25, N'Trong Bóng Chày Nào được chơi trước ?', N'Phân  định bằng đồng xu', N'Đội Khách Chơi  Trước ', N'Chủ Nhà Chơi trước  ', N'Quyết Định Của Trọng Tài    ', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (26, N'Tổng thống john Kenede Bị Ám Sát vào Năm Nào ?', N'1960  ', N'1962     ', N'1963  ', N' 1965', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (27, N'Bài Hát ” thanh Niên Làm Theo lời Bác ” Hay ” Đoàn Ca ” Là Sáng tác Của Ai ?', N'Trần Tiến ', N'Trần hiếu   ', N'Hoàng Hoà', N'không Phải 3 Tác giả Trên    ', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (28, N'Việt Nam  là Thành viên thứ bao nhiêu  của WTO ?', N'140 ', N'145', N'151 ', N'150 ', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (29, N'Tên Dãy núi lớn nhất thế giới ?', N'Andes   ', N'Alps  ', N'Trường Sơn   ', N'Himalaya ', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (30, N'Tên viết tắt  của ngân hàng phát triển châu á ?', N'ADP ', N'UDP', N'ADB   ', N'ASK  ', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (31, N'Đặc điểm nào sau đây đã có ở vượn người ?', N'Đôi Tay Đã Tự Do Khi Đi', N'Đứng Thẳng', N'Biểu Lộ Tình Cảm Vui Buồn', N'Tư Duy Trìu Tượng phức Tạp', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (32, N'Tim người bình thường đập bao nhiêu nhịp một ngày ?', N'50.000    ', N'200.000', N'150.000 ', N'100.000   ', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (33, N'Ca Ghép Thận Thành Công Đầu Tiên ở việt nam thực hiện vào năm nào ?', N'1990 ', N'1991 ', N'1992', N'1993   ', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (34, N'1 phần tử mêtan Có bao nhiêu nguyên hidro ?', N'4', N'3', N'2', N'6', N'A')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (35, N' Giải thưởng ” Cánh Diều vàng” Đã được tổ chức lần đầu tiên Vào năm nào ?', N' 1996  ', N' 1998', N'2002', N'1994', N'D')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (36, N'Thiên Thể Nào lớn đễn Nổi Ngay Cả ánh sángcũng không  thoát khỏi trọng lực của nó ?', N'Hòng Ánh Sáng ', N'Hố Sâu  ', N'Hố Đen', N'Loại Khác  ', N'C')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (41, N'Thủ đô nước Việt Nam?', N'Sài Gòn', N'Hà Nội', N'Đà Nẵng', N'Hải Phỏng', N'B')
GO
INSERT [dbo].[Question] ([MaQT], [Question], [AnswerA], [AnswerB], [AnswerC], [AnswerD], [Answer]) VALUES (43, N'Đất nước nào có hình chữ S nối dài, nằm ở Biển Đông?', N'Thái Lan', N'Brasil', N'Đức', N'Việt Nam', N'D')
GO
SET IDENTITY_INSERT [dbo].[Question] OFF
GO
